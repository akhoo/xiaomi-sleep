# defunct

Not updated anymore.
# xiaomi-sleep
Exporting sleep data from the xiaomi miband 2 then using ggplot to visualise.

# Instructions
Extract the `mi-fit-data-extraction.tar.gz` and follow the instructions in the `README` there to extract the data. It assumes you are using linux, however there are links to the original download files on xda (special thanks to "mibanddd"!).

Then uses `ggplot2` in `R` to visualise the sleeping data as I believe the rest of the data such as deep sleep, heart rate, steps,... are not that important/accurate.

# Limitations
-  The data (probably) does not export time spent awake if going from sleep => awake => sleep.
-  Have to manually modify times that were not automatically tracked properly. 
